package com.mbsystems.deciderflow.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class DeciderFlowBatchConfig {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    public DeciderFlowBatchConfig( JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Tasklet passTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "Success!" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet successTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println("Success");
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet failTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println("Failure");
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Step firstStep() {
        return this.stepBuilderFactory.get( "firstStep" )
                        .tasklet( passTasklet() )
                        .build();
    }

    @Bean
    public Step successStep() {
        return this.stepBuilderFactory.get( "successStep" )
                .tasklet( successTasklet() )
                .build();
    }

    @Bean
    public Step failureStep() {
        return this.stepBuilderFactory.get( "failureStep" )
                        .tasklet( failTasklet() )
                        .build();
    }

    @Bean
    public JobExecutionDecider randomeDecider() {
        return new RandomeDecider();
    }

    @Bean
    public Job job() {
        return this.jobBuilderFactory.get( "DeciderFlowBatch" )
                        .start( firstStep() )
                        .next( randomeDecider() )
                        .from( randomeDecider() )
                        .on( "FAILED" ).to( failureStep() )
                        .from( randomeDecider() )
                        .on( "*" ).to( successStep() )
                        .end()
                        .build();
    }
}
