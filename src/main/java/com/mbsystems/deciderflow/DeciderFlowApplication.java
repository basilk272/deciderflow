package com.mbsystems.deciderflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeciderFlowApplication {

    public static void main( String[] args ) {
        SpringApplication.run( DeciderFlowApplication.class, args );
    }

}
